var Trello = require('node-trello');

var KEY = '99534574545aadcb4fa62b90cd63350c';
var TOKEN = '3866a7ad1e987ed4ee018f3dccaf22bc01c56694e34be457cc8dff310ecebf92';
//var BOARDID = '4f504097889f5cdf13ae9658'; // jwtest
var BOARDID = '50a0c748dc11d540630276cc'; // jwtest

var t = new Trello(KEY, TOKEN);

function dumpAction(a) {
    if (a.data.listAfter) {
        a.type = 'moveCard';
    }
    console.log(a.date + ': ' + a.type + ' by ' + a.memberCreator.fullName);
}

t.get('/1/boards/' + BOARDID + '/actions', {since: '2013-05-14T00:00:00.000Z'}, function(err, data) {
    if (err) throw err;
    data.forEach(dumpAction);
});

