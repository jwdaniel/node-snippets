var Trello = require('node-trello');

var KEY = '99534574545aadcb4fa62b90cd63350c';
var TOKEN = '3866a7ad1e987ed4ee018f3dccaf22bc01c56694e34be457cc8dff310ecebf92';

var t = new Trello(KEY, TOKEN);

/*
t.get('/1/members/me', function(err, data) {
    if (err) throw err;
    console.log(data);
});
*/

function dumpCard(c) {
    var v = '';
    c.labels.forEach(function(l) {
        if (v)
            v = v + ',' + l.name;
        else
            v = l.name;
    });
    console.log(c.idShort + ': (' + v + ') ' + c.name);
}

t.get('/1/members/me', { cards: 'open' }, function(err, data) {
    if (err) throw err;
    data.cards.forEach(dumpCard);
});
