

    server.post('/',function(req, res, next){
        // req.body.pipe(fs.createWriteStream('test.png'))  // body is not a stream
        /*
        // sync is absolutely bad!
        fs.writeFileSync("test.png",req.body,"binary",function(err){
            if(err) throw err;
            res.send("OK")
        });
        */
        fs.createWriteStream('large-received.bin').pipe(req);
    });

