var cb = require('couchbase');
    fs = require('fs');

var CONFIG = 'cb.json';
var cf = JSON.parse(fs.readFileSync(CONFIG));

function process2(bucket) {
    bucket.incr('jw:counter', function(err) {
        if (err) {
            console.log('incr() failed: ' + err);
            return;
        }
        bucket.get('jw:counter', function(err, doc, meta) {
            if (meta)
                console.log('meta = ' + JSON.stringify(meta));
            console.log(doc);
            bucket.shutdown();
        });
    });
}

function process(bucket) {
    // console.log(bucket);
    bucket.get('counter', function(err, doc, meta) {
        if (!doc)
            doc = {count: 0};
        if (meta)
            console.log('meta = ' + JSON.stringify(meta));
        doc.count++;
        console.log(doc);
        bucket.set('counter', doc, meta, function(err) {
            if (err)
                console.log('update failed: ' + err);
        });
    });
}

cb.connect(cf, function(err, bucket) {
    if (err) {
        console.log("Failed to connect cb: " + err)
    } else {
        process(bucket);
        process2(bucket);
    }
});

