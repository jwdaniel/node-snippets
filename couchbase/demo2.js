var async = require("async"),
    fs = require('fs');
var cb = require("couchbase");

var CONFIG = 'cb.json';
var cf = JSON.parse(fs.readFileSync(CONFIG));

cb.connect(cf, function(err, bucket) {
    if (err) {
        console.log("Can't connect to CB:[" + cf.bucket + "]", err);
        process.exit(1);
    }

    var docs = [];
    for (var i = 0; i < 1000; i++) {
        docs.push({id: i});
    }

    async.map(docs, function(doc, callback) {
        bucket.set(doc.id, doc, {}, function(err) {
            // console.log('saving id = ' + doc.id);
            callback(err);
        });
    }, function(err, results) {
        if (err) {
            console.log("too many async ops: ", err);
            process.exit(1);
        }

        console.log("save ok");
        process.exit(0);
    });
});

