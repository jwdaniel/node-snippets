var FogBugz = require('./fogbugz');

FB_URL = 'http://172.28.12.47/fogbugz/api.asp';
FB_TOKEN = '7v6p38e7b1pubnp4qhhdq59n5k0m72';

var fb = new FogBugz({endpoint: FB_URL, token: FB_TOKEN});

function dumpAllProjects() {
    fb.allProjects(function(msg, projects) {
        projects.project.forEach(function(o) {
            console.log(o.sProject + ' / ' + o.sPersonOwner);
        });
    });
}

function dumpCases(ids) {
    var cols = 'ixBug,sTitle,sProject,sStatus,events';
    fb.cases(ids, cols, 10, function(msg, cases) {
        for (i = 0; i < cases.case.length; i++) {
            o = cases.case[i];
            console.log(o.ixBug + ': (' + o.sStatus + ') ' + o.sTitle + ' / ' + o.sProject);
        }
    });
}

//dumpAllProjects();
dumpCases([3001,3002,3003,3004,3005,3006]);

