var http = require('http'),
    url = require('url'),
    rest = require('restler'),
    fs = require('fs'),
    redis = require("redis");

var testurl = 'http://tcdata1.icetech.com.cn/k.fcgi?s=2013052000&e=2013052023&p=ICE.CME.Aus_Doller.201306&f=1';

function G() {}
G.bindport = 3001;
G.db = redis.createClient();
G.db.on("error", function (err) {
    console.log("Error " + err);
    G.db.quit();
    process.exit(0);
});

function setTCData(qstr, data) {
    var key = 'k.fcgi:' + qstr;
    G.db.set(key, data);
}

function fetchTCData(qstr, callback) {
    var fullqstr = 'http://tcdata1.icetech.com.cn/k.fcgi?' + qstr;
    rest.get(fullqstr).on('complete', function(data) {
        if (data instanceof Error) {
            callback(data, null);
        } else {
            setTCData(qstr, data);
            callback(null, data);
        }
    });
}

function getTCData(qstr, callback) {
    G.db.get('k.fcgi:' + qstr, function(err, res) {
        if (!res) {
            fetchTCData(qstr, callback);
        } else {
            callback(null, res);
        }
    });
}

function main() {
    var options = {
      key: fs.readFileSync(__dirname + '/keys/twitlog-key.pem'),
      cert: fs.readFileSync(__dirname + '/keys/twitlog-cert.pem'),
      ca: fs.readFileSync(__dirname + '/keys/twitlog-ca.pem'),
      windowSize: 1420
    };

    var CHUNKSIZE = 3600;

    //spdy.createServer(options, function(req, res) {
    http.createServer(function(req, res) {
        if (req.method == 'GET' && req.url.indexOf('/k.fcgi?') == 0) {
            var urlobj = url.parse(req.url);
            if (urlobj.query) {
                getTCData(urlobj.query, function(err, data) {
                    console.log('hit: ' + urlobj.query + ' length = ' + data.length);
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    if (err) {
                        res.end('error');
                    } else {
                        var offset = 0;
                        function writeChunk() {
                            if (offset + CHUNKSIZE < data.length) {
                                chunk = data.substr(offset, CHUNKSIZE);
                                offset += CHUNKSIZE;
                                res.write(chunk);
                                process.nextTick(writeChunk);
                            } else {
                                chunk = data.substr(offset);
                                res.write(chunk);
                                res.end();
                            }
                        }
                        writeChunk();
                        //res.write(data);
                        //res.end();
                    }
                });
            } else {
                res.end();
            }
        } else {
            res.writeHead(200);
            res.end();
        }
    }).on('listening', function(e) {
        console.log('Listening on port ' + G.bindport);
    }).on('error', function(e) {
        console.log(e);
    }).listen(G.bindport);
}

main();

