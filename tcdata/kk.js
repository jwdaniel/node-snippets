var http = require('http'),
    spdy = require('spdy'),
    url = require('url'),
    rest = require('restler'),
    fs = require('fs'),
    async = require("async"),
    cb = require("couchbase");

var testurl = 'http://tcdata1.icetech.com.cn/k.fcgi?s=2013052000&e=2013052023&p=ICE.CME.Aus_Doller.201306&f=1';

function G() {}
G.bindport = 3001;
G.CONFIG = 'cb.json';
G.cf = JSON.parse(fs.readFileSync(G.CONFIG));

function setTCData(qstr, data) {
    var key = 'k.fcgi:' + qstr;
    var doc = {data: data};
    G.bucket.set(key, doc, {}, function(err) {
        if (err)
            console.log('Err to save key = ' + key);
        else
            console.log('saving key = ' + key + ' length = ' + data.length);
    });
}

function fetchTCData(qstr, callback) {
    var fullqstr = 'http://tcdata1.icetech.com.cn/k.fcgi?' + qstr;
    rest.get(fullqstr).on('complete', function(data) {
        if (data instanceof Error) {
            callback(data, null);
        } else {
            setTCData(qstr, data);
            callback(null, data);
        }
    });
}

function getTCData(qstr, callback) {
    G.bucket.get('k.fcgi:' + qstr, function(err, doc, meta) {
        if (!doc) {
            fetchTCData(qstr, callback);
        } else {
            callback(null, doc.data);
        }
    });
}

function main() {
    var options = {
      key: fs.readFileSync(__dirname + '/keys/twitlog-key.pem'),
      cert: fs.readFileSync(__dirname + '/keys/twitlog-cert.pem'),
      ca: fs.readFileSync(__dirname + '/keys/twitlog-ca.pem'),
      windowSize: 1420
    };

    var CHUNKSIZE = 3600;

  qq='s=2013052000&e=2013052023&p=ICE.CME.Aus_Doller.201306&f=1';
  getTCData(qq, function(err, data) {
    //spdy.createServer(options, function(req, res) {
    http.createServer(function(req, res) {
                    console.log('hit: ' + qq + ' length = ' + data.length);
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    if (err) {
                        res.end('error');
                    } else {
                        var offset = 0;
                        function writeChunk() {
                            if (offset + CHUNKSIZE < data.length) {
                                chunk = data.substr(offset, CHUNKSIZE);
                                offset += CHUNKSIZE;
                                res.write(chunk);
                                process.nextTick(writeChunk);
                            } else {
                                chunk = data.substr(offset);
                                res.write(chunk);
                                res.end();
                            }
                        }
                        writeChunk();
                        //res.write(data);
                        //res.end();
                    }
    }).on('listening', function(e) {
        console.log('Listening on port ' + G.bindport);
    }).on('error', function(e) {
        console.log(e);
    }).listen(G.bindport);
  });
}

cb.connect(G.cf, function(err, bucket) {
    if (err) {
        console.log("Can't connect to CB:[" + cf.bucket + "]", err);
        process.exit(1);
    }
    G.bucket = bucket;

    main();
});

