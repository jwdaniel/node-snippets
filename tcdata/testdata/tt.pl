#!/usr/bin/perl
use DB_File;

#$DB_BTREE->{'flags'} = R_DUP ;
tie %H, "DB_File", "201305-ICE.TWF.FITX.201312.bdb", O_RDWR, 0644, $DB_BTREE || die $!;

while (($k, $v) = each %H) {
    print "$k (" . length($k) . ") = $v (" . length($v) . ")\n";
}

untie %H;
