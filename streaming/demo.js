var fs = require('fs');


function test1() {
    var readFrom = fs.createReadStream('largefile.txt');
    readFrom.on('data', function(data) { 
        // a chunk of data
        console.log(data);
    });
    readFrom.on('end', function() {
        console.log("I finished reading the file!");
    });
}

function test2() {
    var writeTo = fs.createWriteStream('output.txt');
    writeTo.write("Hello, file writing world!");
    writeTo.end();
}

function copy(infile, outfile) {
    var readFrom = fs.createReadStream(infile),
    var writeTo = fs.createWriteStream(outfile);

    /*
    readFrom.on('data', function(data) {
        writeTo.write(data);
    });

    readFrom.on('end', function() {
        writeTo.end();
    });
    */
    readFrom.pipe(writeTo);
}

