var net = require('net');
var clients = [];
 
function broadcast(mesg, sender) {
    clients.forEach(function(c) {
        if (c === sender) return;
        c.write(mesg);
    });
    process.stdout.write(mesg)
}

net.createServer(function(sock) {
    sock.name = sock.remoteAddress + ":" + sock.remotePort 
    clients.push(sock);

    sock.write("Welcome " + sock.name + "!\n");
    broadcast(sock.name + " entered!\n", sock);

    sock.on('data', function(data) {
        broadcast(sock.name + "> " + data, sock);
    });

    sock.on('end', function() {
        clients.splice(clients.indexOf(sock), 1);
        broadcast(sock.name + " left!\n");
    });
}).listen(7000);

console.log("Chat-server running on port 7000");

