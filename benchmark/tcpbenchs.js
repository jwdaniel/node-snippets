var net = require('net');
var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

var SERVERPORT = 3000;
var conns = 0;
var errors = 0;

function doit() {
    net.createServer(function(sock) {
        conns++;
        sock.on('end', function() {
            conns--;
        });
        sock.write('hello~~\n');
        sock.pipe(sock);
    }).on('listening', function() {
        console.log('Listening on port ' + SERVERPORT);
    }).on('error', function(e) {
        errors++;
    }).listen(SERVERPORT, '127.0.0.1');
}

if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
    cluster.on('death', function(worker) {
        console.log('worker ' + worker.pid + ' died');
    });
    setInterval(function() {
        console.log('conns: ' + conns + '  errors: ' + errors);
    }, 1000);
} else {
    doit();
}

