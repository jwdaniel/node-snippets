var net = require('net');

var conns = 0;
var succs = 0;
var errors = 0;

var ipushconn = function() {
    var self = this;

    self.id = 0;
    self.state = 0;
    self.conn = null;
    self.sock = null;
    self.loops = 0;
};

ipushconn.prototype.dotest = function(host, port) {
    var self = this;
    self.conn = net.connect(port, host, function() {
        self.state = 1;
        conns++; succs++;
        self.id = conns;
    });
    self.conn.on('xlogin', function() {
        //process.nextTick(function() {
            self.conn.write('forward ipush.abc.com 172.28.11.143\r\n');
            self.conn.write('xlogin ICE iPush CEO 12345\r\n');
            self.state = 2;
        //});
    });
    self.conn.on('xsub', function() {
        process.nextTick(function() {
            self.conn.write('xsub 41424344\r\n');
            self.state = 3;
        });
    });
    self.conn.on('xquit', function() {
        process.nextTick(function() {
            self.conn.write('xquit\r\n');
            conns--;
            self.state = 4;
        });
    });
    self.conn.on('data', function(data) {
        // console.log('(' + self.id + ':' + self.loops + ') ' + data.toString().substr(0,10));
        switch (self.state) {
        case 0:
        case 1:
            //if (data.toString().substr(1,4) == 'init') {
                self.conn.emit('xlogin');
            //}
            break;
        case 2:
            self.conn.emit('xsub');
            break;
        case 3:
           // if (data.toString().substr(0,4) == 'ABCD') {
                if (self.loops++ >= 5) {
                    self.conn.removeAllListeners('data');
                    self.conn.emit('xquit');
                }
           // }
            break;
        default:
            self.conn.removeAllListeners('data');
            //self.conn.emit('xquit');
            //console.log('(' + self.id + ':' + self.loops + ') ' + data.toString().substr(0,10));
        }
    });

    self.conn.on('error', function(e) {
        // self.conn.end();
        errors++;
        // conns--;
        // console.log('(' + self.id + ') ' + e);
    });

};

var IP = '172.28.11.144';
var PORT = 444;
var TRIES = 10;

if (process.argv.length >= 5) {
    IP = process.argv[2];
    PORT = parseInt(process.argv[3]);
    TRIES = parseInt(process.argv[4]);
}

var n = 0;
var r = 100;

var writeStats = function() {
    v = succs + errors;
    if (v < TRIES) {
        var c = new ipushconn();
        c.dotest(IP, PORT);
        if (v >= r) {
            r += 100;
            console.log('conns: ' + conns + '  succs = ' + succs + '  errors = ' + errors);
        }
        setTimeout(writeStats, 2);
    } else {
        if (conns <= 0) {
            console.log('conns: ' + conns + '  succs = ' + succs + '  errors = ' + errors);
            process.exit(0);
        } else {
            console.log('conns: ' + conns + '  succs = ' + succs + '  errors = ' + errors);
            setTimeout(writeStats, 1000);
        }
    }
};

writeStats();

