var net = require('net');

var TRIES = 6000;
var SERVERIP = '127.0.0.1';
var SERVERPORT = 3000;

var conns = 0;

function connectToServer(ip, port) {
    conn = net.createConnection(port, ip);
    conn.on('connect', function() {
        conns++;
    });
    conn.on('end', function() {
        conns--;
    });
}

for (i = 0; i < TRIES; i++) {
    connectToServer(SERVERIP, SERVERPORT);
}

