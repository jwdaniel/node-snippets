var net = require('net');

var SmtpClient = function() {
    var state = 0;
    var conn = null;
    var sock = null;
};

SmtpClient.prototype = require('events').EventEmitter;
SmtpClient.prototype.connect = function(host, port) {
    var self = this;
    self.conn = net.connect(port, host, function() {
        self.state = 1;
    });
    self.conn.on('ehlo', function() {
        self.conn.write('EHLO icetech.com.tw\r\n');
        self.state = 2;
    });
    self.conn.on('mailfrom', function() {
        self.conn.write('MAIL FROM: <jwwang@icetech.com.tw>\r\n');
        self.state = 3;
    });
    self.conn.on('rcptto', function() {
        self.conn.write('RCPT TO: <jwwang@icetech.com.tw>\r\n');
        self.state = 4;
    });
    self.conn.on('quit', function() {
        self.conn.write('quit\r\n');
    });
    self.conn.on('data', function(data) {
        console.log('state = ' + self.state);
        console.log('data = ' + data);
        switch (self.state) {
        case 1:
            self.conn.emit('ehlo');
            break;
        case 2:
            self.conn.emit('mailfrom');
            break;
        case 3:
            self.conn.emit('rcptto');
            break;
        case 4:
        default:
            self.conn.emit('quit');
            break;
        }
    });

    self.conn.on('end', function() {
        console.log('byebye');
    });

    self.conn.on('error', function(e) {
        console.log(e);
    });

};

var c = new SmtpClient();
c.connect('127.0.0.1', 25);

