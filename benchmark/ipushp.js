var net = require('net');

var ipushconn = function(host, port) {
    var state = 0;
    var conn = null;
    var self = this;

    self.conn = net.connect(port, host, function() {
        self.state = 1;
    });
};

ipushconn.prototype.dotest = function(host, port) {
    var self = this;

    self.conn.on('xlogin', function() {
        process.nextTick(function() {
            self.conn.write('forward ipush.abc.com 172.28.11.143\r\n');
            self.conn.write('xlogin ICE iPush CEO 12345\r\n');
            self.state = 2;
        });
    });
    self.conn.on('xsub', function() {
        process.nextTick(function() {
            self.conn.write('xsub 41424344\r\n');
            self.state = 3;
        });
    });
    self.conn.on('producer', function() {
        self.conn.write('ABCDtest123\r\n');
    });
    self.conn.on('data', function(data) {
        switch (self.state) {
        case 1:
            process.nextTick(function() {
                self.conn.emit('xlogin');
            });
            break;
        case 2:
            process.nextTick(function() {
                self.conn.emit('xsub');
            });
            break;
        case 3:
            self.conn.removeAllListeners('data');
            self.state = 4;
            setInterval(function() {
                self.conn.emit('producer');
            }, 500);
            break;
        default:
            // console.log('Got: ' + data.toString().substr(0,10));
        }
    });
    self.conn.on('error', function(e) {
        errors++;
    });

};

var IP = '172.28.11.144'
var PORT = 444;

if (process.argv.length >= 4) {
    IP = process.argv[2];
    PORT = parseInt(process.argv[3]);
}

var c = new ipushconn(IP, PORT);
c.dotest(IP, PORT);

