(function() {
    var cb = require('couchbase');
    var KEYPREFIX = 'ICE.MCO.';
    var STATUS = ['INACTIVE', 'ACTIVE'];
    var INACTIVE = 0;
    var ACTIVE = 1;
    var TIMEOUT = 2200;

    var MCO = (function() {
        var nodename = 'MCO';
        var cbcfg = null;
        var cbbucket = null;
        var jobs = {};

        var createAgent = function(cfg, callback) {
            if (cfg != null && typeof cfg === 'object') {
                nodename = cfg.nodename;
                cbcfg = cfg.cbcfg;
                jobs = cfg.jobs;
                initcb(callback);
            }
        };

        var initcb = function(callback) {
            if (cbcfg == null) return;

            cb.connect(cbcfg, function(err, bucket) {
                cbbucket = bucket;
                if (err) {
                    console.log("Can't connect to CB:" + cbcfg.bucket, err);
                    for (j in jobs) {
                        jobs[j].status = INACTIVE;
                    }
                    callback(err);
                } else {
                    callback(null, bucket);
                }
            });
        };

        var getStatusMsg = function(jobname) {
            if (jobs[jobname] != null && jobs[jobname].status >= 0) {
                return STATUS[jobs[jobname].status];
            } else {
                return STATUS[0];
            }
        };

        var checkandset = function(j, callback) {
            var job = jobs[j];
            cbbucket.get(KEYPREFIX + job.name, function(err, doc, meta) {
                ts = Date.now();
                if (!doc) {
                    t = {nodename: nodename, timestamp: ts};
                    doc = {v: [t]};
                    job.pos = 0;
                } else if (job.pos >= 0 && (doc.v[job.pos].nodename == nodename)) {
                    doc.v[job.pos] = {nodename: nodename, timestamp: ts};
                } else {
                    for (i = 0, job.pos = -1; i < job.max; i++) {
                        if (doc.v[i] == null) {
                            doc.v[i] = {nodename: nodename, timestamp: ts};
                            job.pos = i;
                            break;
                        } else if (ts - doc.v[i].timestamp >= TIMEOUT) {
                            doc.v[i] = {nodename: nodename, timestamp: ts};
                            job.pos = i;
                            break;
                        }
                    }
                    if (job.pos < 0) {
                        job.status = INACTIVE;
                        callback(err, job);
                        return;
                    }
                }
                doc.v = doc.v.slice(0, job.max);
                console.log(job.name + ': ' + JSON.stringify(doc));
                cbbucket.set(KEYPREFIX + job.name, doc, meta, function(err) {
                    if (err) {
                        job.pos = -1;
                        job.status = INACTIVE;
                    } else {
                        job.status = ACTIVE;
                    }
                    callback(err, job);
                });
            });
        };

        var poll = function(callback) {
            if (cbbucket == null) {
                for (j in jobs) {
                    jobs[j].status = INACTIVE;
                }
                callback('couchbase not connected');
                initcb();
                return;
            } else {
                for (j in jobs) {
                    checkandset(j, function(err) {
                        callback(err);
                    });
                }
            }
        };
    
        return {
            createAgent: createAgent,
            getStatusMsg: getStatusMsg,
            poll: poll,
        };
    })();
    
    module.exports = MCO;
})();

