var cb = require('couchbase');

(function() {
    var KEY = 'ICE.Co';
    var KEYLOCK = 'ICE.Co.Lock';
    var INACTIVE = 0;
    var ACTIVE = 1;
    var STATUS = ['INACTIVE', 'ACTIVE'];

    var Co = (function() {
        var status = INACTIVE;
        var nodename = 'Co';
        var cbcfg = null;
        var cbbucket = null;
        var _start_service = function() { return true; };
        var _stop_service = function() { return true; };
        var _check_service = function() { return status; };

        var createAgent = function(cfg, callback) {
            if (cfg != null && typeof cfg === 'object') {
                nodename = cfg.nodename;
                cbcfg = cfg.cbcfg;
                _start_service = cfg.start;
                _stop_service = cfg.stop;
                _check_service = cfg.check;

                initcb(callback);
            }
        }

        var initcb = function(callback) {
            if (cbcfg == null) return;

            cb.connect(cbcfg, function(err, bucket) {
                cbbucket = bucket;
                if (err) {
                    console.log("Can't connect to CB:" + cbcfg.bucket, err);
                    status = INACTIVE;
                    callback(err);
                } else {
                    callback(null, bucket);
                }
            });
        }

        var getStatusMsg = function() {
            return STATUS[status];
        }

        var kick = function(callback) {
            if (cbbucket == null) {
                status = INACTIVE;
                callback('couchbase not connected');
                initcb();
                return;
            } else {
                cbbucket.get(KEY, function(err, doc, meta) {
                    ts = Date.now();
                    if (!doc) {
                        doc = {nodename: nodename, timestamp: ts};
                    } else if (ts - doc.timestamp >= 1200) {
                        doc = {nodename: nodename, timestamp: ts};
                    } else if (doc.nodename == nodename) {
                        doc = {nodename: nodename, timestamp: ts};
                    } else {
                        status = INACTIVE;
                        callback(err);
                        return;
                    }
                    console.log(doc);
                    cbbucket.set(KEY, doc, meta, function(err) {
                        if (err) {
                            status = INACTIVE;
                        } else {
                            status = ACTIVE;
                        }
                        callback(err);
                    });
                });
            }
        };
    
        var start = function(callback) {
            _start_service(function(err, cb) {
                if (err) {
                } else {
                    cb(null);
                }
            });
        };
    
        var stop = function() {
            console.log(status);
        };

        return {
            createAgent: createAgent,
            getStatusMsg: getStatusMsg,
            kick: kick,
            start: start,
            stop: stop
        };
    })();
    
    module.exports = Co;
})();

