var fs = require('fs'),
    express = require('express'),
    app = express();
var Co = require('./mco');
var cf = JSON.parse(fs.readFileSync('cb.json'));
var jobs = JSON.parse(fs.readFileSync('jobs.json'));

var IP = '172.28.11.143';
var PORT = 3000;
if (process.argv.length >= 4) {
    IP = process.argv[2];
    PORT = parseInt(process.argv[3]);
}

function watchdog() {
    Co.poll(function(err) {
        if (err)
            console.log(Co.getStatusMsg() + ': ' + err);
/*
        else {
            for (j in jobs) {
                console.log(j + ': ' + Co.getStatusMsg(j));
            }
        }
*/
    });
}

Co.createAgent({
    nodename: IP,
    cbcfg: cf,
    jobs: jobs,
}, function(err, bucket) {
    // if (err) return;

    app.use(express["static"](__dirname + '/public'));
    app.use(express.directory(__dirname + '/public'));
    app.use(express.errorHandler());
    // app.use(express.logger());
    app.get('/api/status', function(req, res){
        res.send(Co.getStatusMsg());
    });
    app.use(function(req, res, next) {
        console.log('%s %s', req.method, req.url);
        next();
    });
    app.use(app.router);
    app.listen(PORT);
    console.log('Server listening on port ' + PORT);

    setInterval(watchdog, 1000);
});

