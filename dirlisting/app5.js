var connect = require('connect');
var dirlist = require('dirlist');

var docbase = '/btrfs';

connect(
    connect.favicon(),
    dirlist(docbase),
    connect.static(docbase)
).listen(3002);

