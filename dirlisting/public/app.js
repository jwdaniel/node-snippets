var ns = require('node-static');
var file = new ns.Server('/root');

require('http').createServer(function(req, resp) {
    // req.addListener('end', function() {
        console.log(req.url);
        file.serve(req, resp, function(err, result) {
            if (err) {
                console.log("Error serving " + req.url + " - " + err.message);
                resp.writeHead(err.status, err.headers);
                resp.end();
            }
        });
    // }).resume();
}).listen(3001);
