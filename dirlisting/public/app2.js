var express = require('express');

    var app = express();

    // Configuration
    app.configure(function() {
        app.use(express.static('public/'));
    });

    appServer = app.listen(3001, function() {
        console.log("Express server listening on port %d in %s mode", appServer.address().port, app.settings.env);
    });

