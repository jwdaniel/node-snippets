var express = require('express');
var app = express();


var docbase = __dirname;

app.configure(function() {
    app.use(express.static(docbase, {maxAge: 5*60*1000}));
    app.use(express.directory(docbase));
    app.use(express.errorHandler());
});

appServer = app.listen(3001, function() {
    console.log("Express server listening on port %d in %s mode", appServer.address().port, app.settings.env);
});

