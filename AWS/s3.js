var AWS = require('aws-sdk');
AWS.config.loadFromPath('aws.json');

var s3 = new AWS.S3();

s3.listBuckets(function(err, data) {
    for (var index in data.Buckets) {
        var bucket = data.Buckets[index];
        console.log("Bucket: ", bucket.Name, ' : ', bucket.CreationDate);
    }
});

/*
s3.createBucket({Bucket: 'myBucket'}, function() {
    var params = {Bucket: 'myBucket', Key: 'myKey', Body: 'Hello!'};
    s3.putObject(params, function(err, data) {
        if (err)
          console.log(err)
        else
          console.log("updated successfully!");
    });
});
*/
