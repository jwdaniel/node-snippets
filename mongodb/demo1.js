var assert = require('assert');
var mongodb = require('mongodb');

var mydata = [
    { 'name': 'user1', 'email': 'user1@icetech.com.tw' },
    { 'name': 'user1', 'email': 'user1@icetechnology.com' },
    { 'name': 'user2', 'email': 'user2@icetech.com.tw' },
    { 'name': 'user3', 'email': 'user3@icetech.com.tw' }
];

var db = new mongodb.Db('demodb', new mongodb.Server("127.0.0.1", 27017, {}));
db.open(function(err, db) {
    db.dropDatabase(function(err, done) {
        db.createCollection('demotable', function(err, collection) {
            collection.insert(mydata, function(err, docs) {
                collection.find({'name':'user1'}, function(err, cursor) {
                    cursor.toArray(function(err, items) {
                        console.log(items);
                        // assert.equal(items.length, 1, 'not equal..');
                        db.close();
                    });
                });
            });
        });
    });
});
