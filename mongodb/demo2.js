var mongodb = require('mongodb');

var s = new mongodb.Server('127.0.0.1', 27017, {auto_reconnect: true, poolSize: 10});
var db = new mongodb.Db('demodb', s);

db.open(function(err, db) {
    db.collection("demotable", function(err, table) {
        table.find(function(err, res) {
            console.log(res);
            res.toArray(function(err, items) {
                //console.log(items);
                db.close();
            });
        });
    });
});

