var fs = require('fs');

var data = new Buffer(1024);

function test1() {
    fs.open('out1.txt', 'w', function(err, fd) {
        if (err) throw err;
        fs.write(fd, data, 0, 1024, 0, function(err) {
            if (err) throw err;
            fs.close(fd);
        });
    });
}

function test2() {
    var out = fs.createWriteStream('out2.txt', {'flags': 'w'});
    out.write(data);
    out.end();
}

function test3() {
    fs.writeFile('out3.txt', data, function(err) {
        if (err) throw err;
    });
}

test1();
test2();
test3();

