var fs =  require('fs');
var Emitter = require('events').EventEmitter;
var e = new Emitter();

var done = false;

e.on('byebye', function() {
    done = true;
});

/*
setTimeout(function() {
    process.exit(0);
}, 2000);
*/

setTimeout(function() {
    e.emit('byebye');
}, 200);

var n = 0;
while (!done) {
    process.nextTick(function() {
        console.log('nextTick: ' + n);
    });
    var res = fs.readFileSync('/etc/passwd', "utf8");
    console.log(res.length);
    if (++n >= 200)
        done = true;
}

console.log('exit!');

