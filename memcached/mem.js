var MMC = require('memcached'),
    memcached = new MMC('127.0.0.1');

db.on("error", function (err) {
    console.log("Error " + err);
});

// string
db.set("key1", "value1", redis.print);
db.incr('jw:counter', function(err, res) {
    db.get('jw:counter', function(err, res) {
        console.log('jw:counter = ' + JSON.stringify(res));
    });
});

// hash
db.hset("hash1", "field1", "value1");
db.hset(["hash2", "field2", "value2"], function(err) {
});

db.hgetall("hash2", function(err, res) {
    console.log('hash2 = ' + JSON.stringify(res));
    db.quit();
});

