var https = require('https'),
    fs = require('fs');

var options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

var port = 30443;

https.createServer(options, function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('hello SSL\n');
}).on('listening', function(e) {
    console.log('Listening on port ' + port);
}).on('error', function(e) {
    console.log(e);
}).listen(port);

