var fs = require('fs');
var util = require('util');
var express = require('express');
var app = express();

app.get('/hello.txt', function(req, res) {
    /*
    var body = 'hello world';
    res.setHeader('Content-Type', 'text/plain');
    res.setHeader('Content-Length', body.length);
    res.end(body);
    */

    /*
    res.download('/etc/passwd');
    */

    var rs = fs.createReadStream('/etc/passwd');

    rs.on('open', function() {
        rs.pipe(res);
    });
    rs.on('error', function(err) {
        res.end(err);
    });
});

var port = 3000;
app.listen(port);
console.log('Express listening on port ' + port);

