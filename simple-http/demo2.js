var express = require('express'),
    app = express();

app.get('/', function(req, res){
    res.send('hello~~');
});

app.get('/:file(*)', function(req, res, next) {
    var file = req.params.file;

    res.download('/tmp/' + file);
});

app.configure(function(){
    app.use(express.cookieDecoder());
    app.use(express.logger());
    //app.use(connect.session({ store: new RedisStore({ maxAge: 10080000 }) }));
    app.use(express.session());
    app.use(auth( [
        auth.Facebook({appId : fbId, appSecret: fbSecret, scope: "email", callback: fbCallbackAddress})
    ]) );
    app.use(function(err, req, res, next){
        if (404 == err.status) {
            res.statusCode = 404;
            res.send('Cant find that file, sorry!');
        } else {
            next(err);
        }
    });
});

app.listen(30080, '127.0.0.1');

