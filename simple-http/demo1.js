var http = require('http');

http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('hello~~\n');
}).on('listening', function(e) {
    console.log('Listening at http://127.0.0.1:30080/');
}).on('error', function(e) {
    console.log(e);
}).listen(80, '127.0.0.1');

