var winston = require('winston');

require('winston-riak').Riak;
require('winston-mongo').Mongo;

var logger = new (winston.Logger)({
    transports: [
        //new winston.transports.Console()
        new winston.transports.File({ filename: 'all.log' })
        new winston.transports.MongoDB({ db: 'db', level: 'info'})
        // new winston.transports.Couchdb({ 'host': 'localhost', 'db': 'logs' })
        // new winston.transports.Riak({ bucket: 'logs' })
    ]
    exceptionHandlers: [
        new winston.transports.File({ filename: 'exceptions.log' })
    ]
});

logger.warn('this is a warning.');
logger('info', 'this is an info.');

