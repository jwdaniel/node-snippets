var m = require('./DemoModule');

var cfg = {'data': '123'};

m.createInstance(cfg, function() {
    console.log(m.getValue());
    var cfg2 = {'data': '456'};
    m.createInstance(cfg2, function() {
        console.log(m.getValue());
    });
});


//for (var key in Object.keys(require.cache)) {
    // console.log(key);
    // console.log(require.cache[key]);
    // delete require.cache[key];
//}

var unload = function(modname) {
    var pathname = require.resolve(modname);
    if (require.cache[pathname]) {
        delete require.cache[pathname];
        return true;
    }
    return false;
};

var h = setTimeout(function() {
    console.log(require.cache);
    if (unload('./DemoModule')) {
        console.log(require.cache);
        console.log(m.getValue());
        console.log(require.cache);
        // clearInterval(h);
    }
}, 1000);

