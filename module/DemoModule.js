(function() {
    var DemoModule = (function() {
        var version = "1.0";
        var dmcfg = null;

        var createInstance = function(cfg, callback) {
            if (cfg != null && typeof cfg === 'object') {
                dmcfg = cfg;
                init(callback);
            }
        }

        var init = function(callback) {
            if (dmcfg == null) return;
            callback();
        }

        var getValue = function() {
            return dmcfg.data;
        }

        return {
            createInstance: createInstance,
            getValue: getValue
        };
    })();
    
    module.exports = DemoModule;
})();

