function User(name) {
    this.name = name;
    this.email = '';
}

User.prototype.setEmail = function(email) {
    this.email = email;
}

User.prototype.about = function() {
    console.log(this.name + ': ' + this.email);
}

module.exports = User;

var version = '1.0';
var usage = function() {
    console.log('this is usage()');
}

module.exports.version = version;
module.exports.usage = usage;

