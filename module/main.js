var User = require('./User');

var a = new User('user1');
var b = new User('user2');
b.setEmail('user2@icetech.com.tw');

a.about();
b.about();

console.log('Class [User] version = ' + User.version);
User.usage();

