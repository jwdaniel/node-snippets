var chunk = "";
client.on('data', function(data) {
    chunk += data.toString();
    d_index = chunk.indexOf("\n");

    while (d_index > -1) {
        try {
            string = chunk.substring(0,d_index);
            json = JSON.parse(string);
            process(json);
        }
        chunk = chunk.substring(d_index+1);
        d_index = chunk.indexOf("\n");
    }
});

